function [ mu,sigma,newN ] = EKFSLAM( muold,sigmaold,u,z,N,t ) %mew, covariance(sigmaold), movement(u), bpos(Ntag), s(unique val), dt(t)
%EKFSLAM Without Correspondences- This function implements EKFSLAM
% Inputs: muold - 3N+3 size array
% sigmaold - (3N+3)(3N+3) size matrix
% N - the number of beacons
% u - movement of robot
% Outputs:
x = z(1);
y = z(2);

v = u(1);
w = u(2);

theta = muold(3);
Fx = [eye(3) zeros(3,3*N)];
if w == 0
    mu = muold +Fx'*[v*cos(theta)*t;v*sin(theta)*t;0];
else
    mu = muold + Fx'*[-v/w*sin(theta)+v/w*sin(theta+w*t);v/w*cos(theta)-v/w*cos(theta+w*t);w*t];
end
I = eye(3*N+3);

if w == 0
    jacobrobo = [zeros(3,2) [-v*sin(theta);v*cos(theta);0]];
else
    jacobrobo = [zeros(3,2) [-v/w*cos(theta)+v/w*cos(theta+w*t);-v/w*sin(theta)+v/w*sin(theta+w*t);0]];
end
G = I + Fx'*jacobrobo*Fx;

R = 0.2*eye(3);
Q = 0.2^2*eye(3);
sigmasize = size(sigmaold);
sigma = [[G*sigmaold*G'+Fx'*R*Fx;zeros(3,sigmasize(1))] [zeros(sigmasize(2),3);1e100*eye(3)]];

z = [x;y;N]; % Will probably need to change N
%for loop?
zlen = length(x);
for a = 1:zlen  % for all observed features
    %initialize
    mu(1+(a)*3) = mu(1) + [z(1*a)*cos(z(2*a)+mu(3))]; % mu(1+N*3) = mj,x
    mu(2+(a)*3) = mu(2) + [z(1*a)*sin(z(2*a)+mu(3))]; % mu(2+N*3) = mj,y
    mu(3+(a)*3) = N(a) + 0; % mu(3+N*3) = mj,s
    
    for k = 0:N 
        del = [mu(1+3*k)-mu(1);mu(2+(k-1)*3)-mu(2)]; % relative position of landmark to robot   First time around, should be mu(1)-mu(1)
        q = del'*del;
        
        zt = [sqrt(q);atan2(del(2),del(1))-mu(3);mu(3+(k-1)*3)];
        
        Fxj = [[eye(3);zeros(3,3)] [zeros(6,(k-1)*3-3)] [zeros(3,3);eye(3)] [zeros(6,3*N-(k-1)*3)]];
        
        H = (1/q)*[-sqrt(q)*del(1) -sqrt(q)*del(2) 0 sqrt(q)*del(1) sqrt(q)*del(2) 0;del(2) -del(1) -1 -del(2) del(1) 0;0 0 0 0 0 1]*Fxj;
        
        Psi = H*sigma*H'+Q;
        pie(k) = (z(a,:)'-zt)'*inv(Psi)*(z(a,:)'-zt);
    end
    alpha = 0;
    pie(N+1) = alpha;
    j(a) = find(pie == min(pie)); % argmin
    
    newN = max(N,j(a));
    K = sigma*H(j(a))'*inv(Psi(j(a)));
    mu(3*newN+1:3*newN+3) = mu(3*newN+1:3*newN+3) + K(3*newN+1:3*newN+3,3*newN+1:3*newN+3)*([z(a*3-2) z(a*3-1) z(a*3)]'-zt(j(a),:));
    mu(3*newN+3) = newN;
    sigma(3*newN+1:3*newN+3,3*newN+1:3*newN+3) = (I - K(3*newN+1:3*newN+3,3*newN+1:3*newN+3)*H(j(a)))*sigma(3*newN+1:3*newN+3,3*newN+1:3*newN+3);
    %end for loop
end
end

