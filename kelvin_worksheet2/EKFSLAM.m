function [ mu,sigma,newN ] = EKFSLAM( muold,sigmaold,u,z,N,t ) %mew, covariance(sigmaold), movement(u), bpos(Ntag), s(unique val), dt(t)
%EKFSLAM Without Correspondences- This function implements EKFSLAM
% Inputs: muold - 3N+3 size array
% sigmaold - (3N+3)(3N+3) size matrix
% N - the number of beacons
% u - movement of robot
% Outputs:
x = z(1);
y = z(2);

v = u(1);
w = u(2);

theta = muold(3);
Fx = [eye(3) zeros(3,3*N)];
if w == 0
    mu = muold' +Fx'*[v*cos(theta)*t;v*sin(theta)*t;0];
else
    mu = muold' + Fx'*[-v/w*sin(theta)+v/w*sin(theta+w*t);v/w*cos(theta)-v/w*cos(theta+w*t);w*t];
end
I = eye(3*N+3);
jacobrobo = [zeros(3,2) [-v/w*cos(theta)+v/w*cos(theta+w*t);-v/w*sin(theta)+v/w*sin(theta+w*t);0]];
G = I + Fx'*jacobrobo*Fx;

R = 0.2*eye(3);
Q = 0.2^2*eye(3);
sigma = G*sigmaold*G' + Fx'*R*Fx;

z = [x;y;N]; % Will probably need to change N
%for loop?
zlen = length(x);
for a = 1:zlen  % for all observed features
    %initialize
    mu(1+N(a)*3) = mu(1) + [z(1*a)*cos(z(2*a)+mu(3))]; % mu(1+N*3) = mj,x
    mu(2+N(a)*3) = mu(2) + [z(1*a)*sin(z(2*a)+mu(3))]; % mu(2+N*3) = mj,y
    mu(3+N(a)*3) = N(a) + 0; % mu(3+N*3) = mj,s
    
    for k = 1:N+1
        del = [mu(1+(k-1)*3)-mu(1);mu(2+(k-1)*3)-mu(2)]; % relative position of landmark to robot
        q = del'*del;
        
        zt = [sqrt(q);atan2(del(2),del(1))-mu(3);mu(3+(k-1)*3)];
        
        Fxj = [[eye(3);zeros(3,3)] [zeros(6,(k-1)*3-3)] [zeros(3,3);eye(3)] [zeros(6,3*N-(k-1)*3)]];
        
        H = (1/q)*[-sqrt(q)*del(1) -sqrt(q)*del(2) 0 sqrt(q)*del(1) sqrt(q)*del(2) 0;del(2) -del(1) -1 -del(2) del(1) 0;0 0 0 0 0 1]*Fxj;
        
        Psi = H*sigma*H'+Q;
        pie(k) = (z(a,:)'-zt)'*inv(Psi)*(z(a,:)'-zt);
    end
    alpha = 0;
    pie(N+1) = alpha;
    j(a) = find(pie == min(pie)); % argmin
    
    newN = max(N,j(a));
    K = sigma*H(j(a))'*inv(Psi(j(a)));
    mu = mu + K*([z(a*3-2) z(a*3-1) z(a*3)]'-zt(j(a),:));
    sigma = (I - K*H(j(a)))*sigma;
    %end for loop
end
end

