% This function draws an ellipse on the plot
% Inputs:
% centroid-center of ellipse
% theta-theta from positive x axis
% major- magnitude of major axes
% minor- magnitude of minor axes

function drawEllipse(centroid,theta,major,minor,color)
    
    %draw ellipse
    t = linspace(0,2*pi);
    x = major*cos(t); 
    y = minor*sin(t);
    %rotate and offsets the ellipse
    xRot = centroid(1) + x.*cos(theta) - y.*sin(theta);
    yRot = centroid(2) + x.*sin(theta) + y.*cos(theta);
    plot(xRot,yRot,'color',color);
    %major axes line
    line([centroid(1),centroid(1) + major*cos(theta)],...
         [centroid(2),centroid(2) + major*sin(theta)],'color',color);
    line([centroid(1),centroid(1) + major*cos(theta+pi)],...
         [centroid(2),centroid(2) + major*sin(theta+pi)],'color',color);
    %minor axes line
    line([centroid(1),centroid(1) + minor*cos(theta+pi/2)],...
         [centroid(2),centroid(2) + minor*sin(theta+pi/2)],'color',color)
    line([centroid(1),centroid(1) + minor*cos(theta+3/2*pi)],...
         [centroid(2),centroid(2) + minor*sin(theta+3/2*pi)],'color',color)
     
     