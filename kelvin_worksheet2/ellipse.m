%% 
clear;
% Mean and Covariance 

%parse textfile
fileId = fopen('cdata.txt','r');
fileId2 = fopen('mdata.txt','r');
numFeature = 3;
covar = fscanf(fileId,'%f',[4*numFeature+4, Inf]);
mean = fscanf(fileId2,'%f',[3*numFeature+3, Inf]);

%Transpose matrix to be a row-wise
covar = covar';
mean = mean';
covarSize = size(covar);

filename = 'SlamTest2.gif';

color = ['y','m','c','r'];

%Starting position
lineSegment = [0,0];

for i=1:covarSize(1)
    fig1 = figure(i);
    fig1.Name = ['Slam ', num2str(i)];
    hold on;
    lineSegment(i+1,:) = [mean(i,1),mean(i,2)];
    for j=1:numFeature + 1
        avg = [mean(i,1 + 3*(j-1)),mean(i,2 + 3*(j-1))];
        
        covariance = [ covar(i, 1 + 4*(j-1)), covar(i, 2 + 4*(j-1));covar(i, 3 + 4*(j-1)) covar(i, 4 + 4*(j-1)) ];
        if ismember(9999,covariance)
            continue;
        end
        [eigenvec, eigenval] = eig(covariance);

        % Get the index of the largest eigenvector
        [max_evc_ind_c, r] = find(eigenval == max(max(eigenval)));
        max_evc = eigenvec(:, max_evc_ind_c);

        % Get the largest eigenvalue
        max_evl = max(max(eigenval));

        % Get the smallest eigenvector and eigenvalue
        if(max_evc_ind_c == 1)
            min_evl = max(eigenval(:,2));
            min_evc = eigenvec(:,2);
        else
            min_evl = max(eigenval(:,1));
            min_evc = eigenvec(1,:);
        end

        % Calculate the angle between the x-axis and the largest eigenvector
        angle = atan2(max_evc(2), max_evc(1));

        % This angle is between -pi and pi.
        % Let's shift it such that the angle is between 0 and 2pi
        if(angle < 0)
            angle = angle + 2*pi;
        end


        % Get the 95% confidence interval error ellipse
        chisquare_val = 2.4477;
        theta_grid = linspace(0,2*pi);
        phi = angle;
        X0  = avg(1);
        Y0  = avg(2);
        a   = chisquare_val*sqrt(max_evl);
        b   = chisquare_val*sqrt(min_evl);
        drawEllipse([X0,Y0],phi,a,b,color(j));
    end
    
    for k=1:i
        line([lineSegment(k,1),lineSegment(k+1,1)],[lineSegment(k,2),lineSegment(k+1,2)],'color','k','LineStyle','--');
    end
    
    frame = getframe(i);

    im = frame2im(frame);

    [imind,cm] = rgb2ind(im,256);
    if i == 1;
        imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
    else
        imwrite(imind,cm,filename,'gif','WriteMode','append');
    end
    
end
% Set the axis labels
hXLabel = xlabel('x');
hYLabel = ylabel('y');

fclose(fileId);
fclose(fileId2);