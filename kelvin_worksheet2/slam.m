function [newMew,newCovar,featureList]=slam(mew,covariance,movement,bpos,s,dt,featureList,numFeature,allFeature)
    mewSize = size(mew);
    N=numFeature;
    Fx = zeros(3,3*N+3);
    for i=1:3
        Fx(i,i) = 1;
    end
    v=movement(1);
    w=movement(2);
    
    if w ~= 0
        newMew = mew' + Fx'*[(-v/w)*sin(mew(3)) + (v/w)*sin(mew(3)+w*dt);...
                            (v/w)*cos(mew(3)) - (v/w)*cos(mew(3)+w*dt);
                             w*dt];
        G = eye(3*N+3) + Fx'*[0,0,-(v/w)*cos(mew(3)) + (v/w)*cos(mew(3)+w*dt);...
                     0,0,(-v/w)*sin(mew(3)) + (v/w)*sin(mew(3)+w*dt);...
                     0,0,0]*Fx;
    else
        newMew = mew' + Fx'*[v*cos(mew(3))*dt;v*sin(mew(3))*dt;0];
        G = eye(3*N+3) + Fx'*[0,0, -v*sin(mew(3));...
             0,0, v*cos(mew(3));...
             0,0,0]*Fx;
    end
    noiseR = [.04,0,0;0,.04,0;0,0,.04];
    noiseQ = [.01,0,0;0,.01,0;0,0,.01];
    newCovar = G*covariance*G' + Fx'*noiseR*Fx;
    %if land mark never seen before
    numS = size(s);
    %j offset
    j=-1;
    for i=1:numS(1)
        j = find(allFeature==s(i))*3+1;
        if ~ismember(s(i),featureList)
            featureList = [featureList,s(i)];
            newMew(j) = newMew(1) + bpos(i,1)*cos(bpos(i,2)+newMew(3));
            newMew(j+1) = newMew(2) + bpos(i,1)*sin(bpos(i,2)+newMew(3));
            newMew(j+2) = s(i);
        end
        Sx = newMew(j) - newMew(1);
        Sy = newMew(j+1) - newMew(2);
        Sxy = [Sx;Sy];
        q = Sxy'*Sxy;
        sqrtQ = sqrt(q);
        newBpos = [sqrtQ;atan2(Sy,Sx) - newMew(3);0];
        Fxj = zeros(6,3*N+3);
        for k=1:3
            Fxj(k,k) = 1;
            Fxj(k+3,k+j-1) = 1;
        end
        H = (1/q)*[-sqrtQ*Sx -sqrtQ*Sy 0 sqrtQ*Sx sqrtQ*Sy 0;...
                 Sy -Sx -q -Sy Sx 0 ;...
                 0 0 0 0 0 q]*Fxj;
        filter = newCovar*H'*inv(H*newCovar*H'+noiseQ);
        newMew = newMew + filter*([bpos(i,:)';0] - newBpos);
        newCovar = (eye(3*N+3) - filter*H)*newCovar;
    end
     newMew = newMew';
        
    
    