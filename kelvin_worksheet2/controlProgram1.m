function finalRad = myFirstControlProgram(robotObj)
    % Set constants for this program
    maxDuration= 1200;  % Max time to allow the program to run (s)    
    
    % Initialize loop variables
    tStart= tic;        % Time limit marker
    
    mewFile = fopen('mdata.txt','w');
    covarFile = fopen('cdata.txt','w');
    %initialize goals and path
    path = [2,3];
    goals = [0.15 1;0.15 4;];
    numLoops = 3;
    loops = -1;
    % Set initial robot location
    initX = -1;
    initY = 2;
    initTheta = 0;
    initPos = [initX initY initTheta];
    [bx,by,s] = ReadBeacon(robotObj);
    setMapStart(robotObj,initPos);
    movementSpeed = 0.25;
    %initialize features
    allFeature = [1,2,3];
    sFeature = size(allFeature);
    numFeature = sFeature(2);
    mew = [initX,initY,initTheta,zeros(1,numFeature*3)];
    covar = eye(3*numFeature+3);
    fFormat = ['%f %f %f'];
    fFormat2 = ['%f %f %f %f'];
    for i=1:numFeature
        fFormat = [fFormat,' %f %f %f'];
        fFormat2 = [fFormat2,' %f %f %f %f'];
    end
    fFormat = [fFormat, '\n'];
    fFormat2 = [fFormat2, '\n'];
    for i=1:3
        covar(i,i) = 0;
    end
    covar = 9999*covar;
    % Start robot driving forward
    SetFwdVelAngVel(robotObj,0,0);
    x = initX;
    y = initY;
    theta = initTheta;
    %display(mew);
    %display(covar);
    % Enter main loop
    featureList = [];
    z = [];
    terminate = 0;
    %turn to first goal
    pathNum = 1;
    mew(3) = turnToGoal(mew(1),mew(2),goals(pathNum,1),goals(pathNum,2),mew(3),robotObj);   
    while(1)
        terminate = 0;
        rStart=tic;
        while (1)
            [distanceR, distanceF, distanceL]=ReadIRMultiple(robotObj);
            [x,y] = moveForward(x,y,theta,movementSpeed,.25,robotObj);
            [bx,by,s] = ReadBeacon(robotObj);
            beaconNum = size(bx);
            for i=1:beaconNum(1)
                z(i,1) = sqrt(bx(i)^2 + by(i)^2);
                z(i,2) = atan2(by(i),bx(i)); 
                if(z(i,1) <= .5)
                    terminate = 1;
                end
            end
            if terminate
                break;
            end
        end
        dt = toc(rStart);
        [mew,covar,featureList]=slam(mew,covar,[movementSpeed,0],z,s,dt,featureList,numFeature,allFeature);
        fprintf(mewFile,fFormat,mew);
        covarData = [];
        for m=1:3:3*numFeature + 3
            covarData = [ covarData, covar(m,m), covar(m,m+1), covar(m+1,m), covar(m+1,m+1)];
        end
        fprintf(covarFile,fFormat2,covarData);
        %display(mew);
        %display(covar);
        %display(featureList);
        sSize = size(s);
        if sSize(1) < 1
            pathNum = 2;
            loops = loops + 1;
            if loops == numLoops;
                break;
            end
        end
        if s == 2
            pathNum = 2;
            loops = loops + 1;
            if loops == numLoops;
                break;
            end
        end
        if s == 3
            pathNum = 1;
        end
        
        mew(3) = turnToGoal(mew(1),mew(2),goals(pathNum,1),goals(pathNum,2),mew(3),robotObj);
    end
    fclose(mewFile);
    fclose(covarFile);
end

function [x,y] = moveForward(x,y,theta,movementSpeed,dist,robotObj)
    SetFwdVelAngVel(robotObj,0,0);
    d = 0;
    d = DistanceSensor(robotObj);
    SetFwdVelAngVel(robotObj,movementSpeed,0);
    while(d <= dist)
        d = d + DistanceSensor(robotObj);
        pause(0.1)
    end
    SetFwdVelAngVel(robotObj,0,0);
    %testing purposes
    x = x + d*cos(theta);
    y = y + d*sin(theta);
end  

function [x,y] = moveBackward(x,y,theta,movementSpeed,dist,robotObj)
    SetFwdVelAngVel(robotObj,0,0);
    d = 0;
    d = DistanceSensor(robotObj);
    SetFwdVelAngVel(robotObj,-movementSpeed,0);
    while(d >= -dist)
        d = d + DistanceSensor(robotObj);
        pause(0.1)
    end
    SetFwdVelAngVel(robotObj,0,0);
    %testing purposes
    x = x + d*cos(theta);
    y = y + d*sin(theta);
end

function theta = turnLeft(theta,robotObj)
    SetFwdVelAngVel(robotObj,0,0);
    TurnAngle(robotObj, .1, pi/2)
    SetFwdVelAngVel(robotObj,0,0);
    theta = theta + pi/2;
end

function theta = turnRight(theta,robotObj)
    SetFwdVelAngVel(robotObj,0,0);
    TurnAngle(robotObj, .1, -pi/2);
    SetFwdVelAngVel(robotObj,0,0);
    theta = theta - pi/2;
end

function theta = turnToGoal(x,y,gx,gy,theta,robotObj)
    SetFwdVelAngVel(robotObj,0,0);
    goalAngle = atan2(gy-y,gx-x);
    %turn to 0 
    TurnAngle(robotObj, .1, -theta);
    TurnAngle(robotObj, .1, goalAngle);
    theta = goalAngle;
end

function w = v2w(v)

% Calculate the maximum allowable angular velocity from the linear velocity
%
% Input:
% v - Forward velocity of Create (m/s)
%
% Output:
% w - Angular velocity of Create (rad/s)
    % Robot constants
    maxWheelVel= 0.5;   % Max linear velocity of each drive wheel (m/s)
    robotRadius= 0.08;   % Radius of the robot (m)

    % Max velocity combinations obey rule v+wr <= v_max
    w= (maxWheelVel-v)/robotRadius;
end